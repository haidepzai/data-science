# Data Science

> Some basic Data Science Implementation based on a Udemy Course by datamics

## Topics

* 1. Linear Regression
* 2. Logistic Regression
* 3. K-Nearest-Neighbors
* 4. Decision Trees and Random Forests
* 5. Support Vector Machines
* 6. K-Means Clustering
* 7. Principal Component Analysis
* 8. Recommender Systems
* 9. Natural Language Processing